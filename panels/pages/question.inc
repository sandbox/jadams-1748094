<?php

/**
 * @file
 * Questions Pages.
 */

$page = new stdClass();
/* Edit this to true to make a default page disabled initially */
$page->disabled = FALSE;
$page->api_version = 1;
$page->name = 'qa_question';
$page->task = 'page';
$page->admin_title = 'Questions & Answers Questions';
$page->admin_description = '';
$page->path = 'qa_question/%qa_question';
$page->access = array();
$page->menu = array();
$page->arguments = array(
  'qa_question' => array(
    'id' => 1,
    'identifier' => 'QA Question: ID',
    'name' => 'entity_id:qa_question',
    'settings' => array(),
  ),
);
$page->conf = array(
  'admin_paths' => FALSE,
);
$page->default_handlers = array();
$handler = new stdClass();
/* Edit this to true to make a default handler disabled initially */
$handler->disabled = FALSE;
$handler->api_version = 1;
$handler->name = 'page_question_panel_context';
$handler->task = 'page';
$handler->subtask = 'question';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Default Question Template',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
  ),
);
$display->cache = array();
$display->title = '%qa_question:title';
$display->content = array();
$display->panels = array();
$pane = new stdClass();
$pane->pid = 'new-1';
$pane->panel = 'center';
$pane->type = 'entity_view';
$pane->subtype = 'qa_question';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
  'view_mode' => 'full',
  'context' => 'argument_entity_id:qa_question_1',
  'override_title' => 0,
  'override_title_text' => '',
);
$pane->cache = array();
$pane->style = array(
  'settings' => NULL,
);
$pane->css = array();
$pane->extras = array();
$pane->position = 0;
$pane->locks = array();
$display->content['new-1'] = $pane;
$display->panels['center'][0] = 'new-1';
$pane = new stdClass();
$pane->pid = 'new-2';
$pane->panel = 'center';
$pane->type = 'views';
$pane->subtype = 'answers';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
  'override_pager_settings' => 0,
  'use_pager' => 1,
  'nodes_per_page' => '0',
  'pager_id' => '0',
  'offset' => '0',
  'more_link' => 0,
  'feed_icons' => 0,
  'panel_args' => 0,
  'link_to_view' => 0,
  'args' => '',
  'url' => '',
  'display' => 'default',
  'context' => array(
    0 => 'argument_entity_id:qa_question_1.id',
  ),
  'override_title' => 1,
  'override_title_text' => 'Answers',
);
$pane->cache = array();
$pane->style = array(
  'settings' => NULL,
);
$pane->css = array();
$pane->extras = array();
$pane->position = 1;
$pane->locks = array();
$display->content['new-2'] = $pane;
$display->panels['center'][1] = 'new-2';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-1';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;

