
-- SUMMARY --

Creates a questions and answers area similar to StackOverflow and Yahoo Answers.

Admins Create Sections, which can then have different Question and Answers. Both
Questions and Answers are fieldable allowing the admin to specify which fields.

e.g. - Admin creates a two sections, Cars and Boats. Questions/Answer in the
Cars Section can have totally different fields than those in the Boats section.

Sections also have the ability to limit by role user that are able to ask a 
question and users that are able to ask a question. Users also have the ability
to vote on answers by way of the voting api, as well as the ability to chose the
best answer.

-- CONFIGURATION --

You can configure Sections Entities at admin/structure/qa-sections and new 
Sections can be added at admin/structure/qa-sections/add.

Unlike Sections, Question Entities can be broken down into type and each of
these is fieldable. The module comes pre-installed with single default Question
Type but more can be added at admin/structure/qa-question-types. Question types
are then assigned to the Section at the Section add/edit page. Questions can
only be added from the Section view page as each Question needs to be related to
a Section.

Answer Entities function much like Questions. Answers can be broken down into
types and each of these is fieldable. The module comes pre-installed with single
default Answer Type and more can be added at admin/structure/qa-answer-types.
