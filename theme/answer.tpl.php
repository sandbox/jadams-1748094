<?php

/**
 * @file
 * Default theme implementation for Answer Entities.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $role_classes: String of classes that can be used to style contextually
 *   through CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<div id="qa-answer-<?php print $answer->id; ?>" class="qa-answer <?php print $role_classes; ?> <?php $answer->best_answer ? print 'best-answer' : ''; ?>">
	
  <?php if ($answer->best_answer): ?>
    <h2>Best Answer</h2>
  <?php endif; ?>

  <div class="qa-answers-content">
    <?php print render($content); ?>
  </div>

</div>

