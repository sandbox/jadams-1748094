<?php

/**
 * @file
 * Theme implementation for the answer vote widget.
 */
?>

<div class="vote">
  
  <div class="answer-votes"><?php print $votes; ?></div>
    
    <div class="vote-links">
      <div class="vote-up"><?php print l(t('Vote Up'), 'qa/vote/answer/' . $answer->id . '/up', array('attributes' => array('class' => array('use-ajax')))); ?></div>
      <div class="vote-down"><?php print l(t('Vote Down'), 'qa/vote/answer/' . $answer->id . '/down', array('attributes' => array('class' => array('use-ajax')))); ?></div>
    </div>
</div>

