<?php

/**
 * @file
 * Default theme implementation for Section Entities.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $role_classes: String of classes that can be used to style contextually
 *   through CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<div class="qa-section <?php print $role_classes; ?>">
	
  <?php if (!$page && $show_title): ?>
    <h2><?php print l($section->title, $url); ?></h2>
  <?php endif; ?>
	
  <div class="qa-section-content">
    <?php
// Hide questions so that we can render them later.
hide($content['questions']); print render($content);
?>
  </div>
   
  <div class="qa-section-questions">
    <?php print render($content['questions']); ?>
  </div>
</div>

