<?php

/**
 * @file
 * Theme and preprocess functions.
 *
 * TODO: Find a way to avoid calling template_preprocess
 * on each entity.
 */

/**
 * Preprocess function for Section Entities.
 *
 * @param array $vars
 *   Entity variables.
 */
function template_preprocess_qa_section(&$vars) {

  $section = $vars['elements']['#section'];
  $vars['section'] = $section;
  $vars['show_title'] = TRUE;

  // Change view mode to full if viewing the section through a panel page.
  if (module_exists('panels')) {
    $current_panel = panels_get_current_page_display();
    if (isset($current_panel->context['argument_entity_id:qa_section_1']) && $current_panel->context['argument_entity_id:qa_section_1']->data == $section) {
      $vars['show_title'] = FALSE;
    }
  }

  $vars['view_mode'] = $vars['elements']['#view_mode'];
  $vars['page'] = $vars['view_mode'] == 'full';

  // Classes by role.
  $vars['role_classes'] = _qa_entity_classes($section);
  $url = $vars['section']->uri();
  $vars['url'] = $url['path'];

  _qa_preprocess_entity_fields($vars);
}

/**
 * Preprocess function for Question Entities.
 *
 * @param array $vars
 *   Entity variables.
 */
function template_preprocess_qa_question(&$vars) {

  $question = $vars['elements']['#question'];
  $vars['question'] = $question;
  $vars['show_title'] = TRUE;

  // Change view mode to full if viewing the question through a panel page.
  if (module_exists('panels')) {
    $current_panel = panels_get_current_page_display();
    if (isset($current_panel->context['argument_entity_id:qa_question_1']) && $current_panel->context['argument_entity_id:qa_question_1']->data == $question) {
      $vars['show_title'] = FALSE;
    }
  }

  $vars['view_mode'] = $vars['elements']['#view_mode'];
  $vars['page'] = $vars['view_mode'] == 'full';

  // Classes by role.
  $vars['role_classes'] = _qa_entity_classes($question);
  $url = $question->uri();
  $vars['url'] = $url['path'];

  _qa_preprocess_entity_fields($vars);
}

/**
 * Preprocess function for Answer Entities.
 *
 * @param array $vars
 *   Entity variables.
 */
function template_preprocess_qa_answer(&$vars) {

  $answer = $vars['elements']['#answer'];

  $vars['answer'] = $answer;
  $vars['view_mode'] = $vars['elements']['#view_mode'];
  $vars['page'] = $vars['view_mode'] == 'full';

  // Classes by role.
  $vars['role_classes'] = _qa_entity_classes($answer);
  $url = $answer->uri();
  $vars['url'] = $url['path'];

  _qa_preprocess_entity_fields($vars);
}

/**
 * Creates a helpful $content variable for use in templates.
 *
 * @param array $vars
 *   Entity variables.
 */
function _qa_preprocess_entity_fields(&$vars) {

  // Helpful $content variable for templates.
  $vars += array('content' => array());
  foreach (element_children($vars['elements']) as $key) {
    $vars['content'][$key] = $vars['elements'][$key];
  }
}

/**
 * Helper function to grab all roles from the entity.
 *
 * @param object $entity
 *   The entity itself.
 *
 * @return string
 *   String of roles from the entity author.
 *
 */
function _qa_entity_classes($entity) {
  $author = user_load($entity->uid);
  $classes = array();
  foreach ($author->roles as $role) {
    $classes[] = 'role-' . str_replace(' ', '-', $role);
  }
  return implode(' ', $classes);
}

