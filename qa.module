<?php

/**
 * @file
 * Main module file for Questions & Answers.
 *
 * TODO: Add Pathauto Logic
 */

/**
 * Implements hook_entity_info().
 */
function qa_entity_info() {
  $return = array();
  $return['qa_section'] = array(
    'label' => t('QA Section'),
    'entity class' => 'QaSection',
    'controller class' => 'QaSectionController',
    'base table' => 'qa_section',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
    ),
    'bundles' => array(
      'qa_section' => array(
        'label' => 'QA Section',
        'admin' => array(
          'path' => 'admin/structure/qa-sections',
          'access arguments' => array('administer qa sections'),
        ),
      ),
    ),
    'load hook' => 'qa_section_load',
    'view modes' => array(
      'full' => array(
        'label' => t('Default'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label' => t('Teaser'),
        'custom settings' => FALSE,
      ),
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'module' => 'qa',
    'admin ui' => array(
      'path' => 'admin/structure/qa-sections',
      'file' => 'qa.admin.inc',
      'controller class' => 'EntityDefaultUIController',
    ),
    'access callback' => 'qa_section_access',
  );
  $return['qa_question'] = array(
    'label' => t('QA Question'),
    'entity class' => 'QaQuestion',
    'controller class' => 'QaQuestionController',
    'base table' => 'qa_question',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'bundle' => 'type',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'bundles' => array(),
    'load hook' => 'qa_question_load',
    'view modes' => array(
      'full' => array(
        'label' => t('Default'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label' => t('Teaser'),
        'custom settings' => FALSE,
      ),
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'module' => 'qa',
    'access callback' => 'qa_access',
  );
  $return['qa_question_type'] = array(
    'label' => t('QA Question Type'),
    'entity class' => 'QaQuestionType',
    'controller class' => 'QaQuestionTypeController',
    'base table' => 'qa_question_type',
    'fieldable' => FALSE,
    'bundle of' => 'qa_question',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'module' => 'qa',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/structure/qa-question-types',
      'file' => 'qa.admin.inc',
      'controller class' => 'EntityDefaultUIController',
    ),
    'access callback' => 'qa_question_type_access',
  );
  $return['qa_answer'] = array(
    'label' => t('QA Answer'),
    'entity class' => 'QaAnswer',
    'controller class' => 'QaAnswerController',
    'base table' => 'qa_answer',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'bundle' => 'type',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'bundles' => array(),
    'load hook' => 'qa_question_load',
    'view modes' => array(
      'full' => array(
        'label' => t('Default'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label' => t('Teaser'),
        'custom settings' => FALSE,
      ),
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'module' => 'qa',
    'access callback' => 'qa_answer_access',
  );
  $return['qa_answer_type'] = array(
    'label' => t('QA Answer Type'),
    'entity class' => 'QaAnswerType',
    'controller class' => 'QaAnswerTypeController',
    'base table' => 'qa_answer_type',
    'fieldable' => FALSE,
    'bundle of' => 'qa_answer',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'module' => 'qa',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/structure/qa-answer-types',
      'file' => 'qa.admin.inc',
      'controller class' => 'EntityDefaultUIController',
    ),
    'access callback' => 'qa_answer_type_access',
  );
  return $return;
}

/**
 * Implements hook_entity_info_alter().
 */
function qa_entity_info_alter(&$entity_info) {
  foreach (qa_question_types() as $type => $info) {
    $entity_info['qa_question']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/qa-question-types/manage/%qa_question_type',
        'real path' => 'admin/structure/qa-question-types/manage/' . $type,
        'bundle argument' => 4,
      ),
    );
  }
  foreach (qa_answer_types() as $type => $info) {
    $entity_info['qa_answer']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/qa-answer-types/manage/%qa_answer_type',
        'real path' => 'admin/structure/qa-answer-types/manage/' . $type,
        'bundle argument' => 4,
      ),
    );
  }
}

/**
 * Implements hook_entity_property_info_alter().
 */
function qa_entity_property_info_alter(&$info) {

  // Section Entities.
  $info['qa_section']['properties']['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the section was posted."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer sections',
    'schema field' => 'created',
  );
  $info['qa_section']['properties']['changed'] = array(
    'label' => t("Date changed"),
    'type' => 'date',
    'schema field' => 'changed',
    'description' => t("The date the section was most recently updated."),
  );
  $info['qa_section']['properties']['uid'] = array(
    'label' => t("Author"),
    'type' => 'user',
    'description' => t("The author of the section."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer example_task entities',
    'required' => TRUE,
    'schema field' => 'uid',
  );

  // Question Entities.
  $info['qa_question']['properties']['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the question was posted."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer questions',
    'schema field' => 'created',
  );
  $info['qa_question']['properties']['changed'] = array(
    'label' => t("Date changed"),
    'type' => 'date',
    'schema field' => 'changed',
    'description' => t("The date the question was most recently updated."),
  );
  $info['qa_question']['properties']['sid'] = array(
    'label' => t("QA Section"),
    'type' => 'qa_section',
    'description' => t("The section this question belongs to."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer example_task entities',
    'required' => TRUE,
    'schema field' => 'sid',
  );
  $info['qa_question']['properties']['uid'] = array(
    'label' => t("Author"),
    'type' => 'user',
    'description' => t("The author of the question."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer example_task entities',
    'required' => TRUE,
    'schema field' => 'uid',
  );

  // Answer Entities.
  $info['qa_answer']['properties']['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the question was posted."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer questions',
    'schema field' => 'created',
  );
  $info['qa_answer']['properties']['changed'] = array(
    'label' => t("Date changed"),
    'type' => 'date',
    'schema field' => 'changed',
    'description' => t("The date the question was most recently updated."),
  );
  $info['qa_answer']['properties']['qid'] = array(
    'label' => t("QA Question"),
    'type' => 'qa_question',
    'description' => t("The answer this question belongs to."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer example_task entities',
    'required' => TRUE,
    'schema field' => 'qid',
  );
  $info['qa_answer']['properties']['uid'] = array(
    'label' => t("Author"),
    'type' => 'user',
    'description' => t("The author of the question."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer example_task entities',
    'required' => TRUE,
    'schema field' => 'uid',
  );
}

/**
 * Implements hook_extra_fields().
 */
function qa_field_extra_fields() {
  $fields = array();
  $fields['qa_section']['qa_section'] = array(
    'form' => array(
      'title' => array(
        'label' => t('Title'),
        'description' => t('Section Title'),
        'weight' => -100,
      ),
    ),
    'display' => array(
      'author' => array(
        'label' => t('Author'),
        'description' => t('Author'),
        'weight' => -100,
      ),
      'form' => array(
        'label' => t('Question Form'),
        'description' => t('Question Form'),
        'weight' => 999,
      ),
    ),
  );
  foreach (qa_question_types() as $type => $info) {
    $fields['qa_question'][$type] = array(
      'form' => array(
        'title' => array(
          'label' => t('Title'),
          'description' => t('Title'),
          'weight' => -100,
        ),
      ),
      'display' => array(
        'author' => array(
          'label' => t('Author'),
          'description' => t('Author'),
          'weight' => -100,
        ),
        'form' => array(
          'label' => t('Answer Form'),
          'description' => t('Answer Form'),
          'weight' => 999,
        ),
      ),
    );
  }
  foreach (qa_answer_types() as $type => $info) {
    $fields['qa_answer'][$type] = array(
      'form' => array(
        'title' => array(
          'label' => t('Title'),
          'description' => t('Title'),
          'weight' => -100,
        ),
      ),
    );
  }
  return $fields;
}

/**
 * Implements hook_menu().
 */
function qa_menu() {
  $items = array();

  // Sections Menu Items.
  $qa_section_uri = 'qa_section/%qa_section';
  $qa_section_uri_argument_position = 1;

  // I really don't like putting access/page arguments on more than one line but
  // drupal code sniffer is complaining about it since its longer that 80
  // characters.
  $items[$qa_section_uri] = array(
    'title callback' => 'entity_label',
    'title arguments' => array('qa_section', $qa_section_uri_argument_position),
    'page callback' => 'qa_section_view',
    'access arguments' => array('administer qa'),
    'page arguments' => array($qa_section_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'view',
      'qa_section',
      $qa_section_uri_argument_position,
    ),
    'file' => 'qa.pages.inc',
  );

  $items[$qa_section_uri . '/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items[$qa_section_uri . '/delete'] = array(
    'title' => 'Delete Section',
    'title callback' => 'qa_label',
    'title arguments' => array($qa_section_uri_argument_position),
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'qa_section_delete_form',
      $qa_section_uri_argument_position,
    ),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'edit',
      'qa_section',
      $qa_section_uri_argument_position,
    ),
    'file' => 'qa.admin.inc',
  );

  $items[$qa_section_uri . '/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'qa_section_form',
      $qa_section_uri_argument_position,
    ),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'edit',
      'qa_section',
      $qa_section_uri_argument_position,
    ),
    'file' => 'qa.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  // Questions Menu Items.
  $qa_question_uri = 'qa_question/%qa_question';
  $qa_question_uri_argument_position = 1;

  $items[$qa_question_uri] = array(
    'title callback' => 'entity_label',
    'title arguments' => array(
      'qa_question',
      $qa_question_uri_argument_position,
    ),
    'page callback' => 'qa_question_view',
    'page arguments' => array($qa_question_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'view',
      'qa_question',
      $qa_question_uri_argument_position,
    ),
    'file' => 'qa.pages.inc',
  );

  $items[$qa_question_uri . '/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items[$qa_question_uri . '/delete'] = array(
    'title' => 'Delete Question',
    'title callback' => 'qa_label',
    'title arguments' => array($qa_question_uri_argument_position),
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'qa_delete_form',
      $qa_question_uri_argument_position,
    ),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'edit',
      'qa_question',
      $qa_question_uri_argument_position,
    ),
    'file' => 'qa.admin.inc',
  );

  $items[$qa_question_uri . '/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'qa_question_form',
      $qa_question_uri_argument_position,
    ),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'edit',
      'qa_question',
      $qa_question_uri_argument_position,
    ),
    'file' => 'qa.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  $items['admin/structure/qa-question-types/%qa_question_type/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('qa_question_type_form_delete_confirm', 4),
    'access arguments' => array('administer qa question types'),
    'weight' => 1,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'qa.admin.inc',
  );

  // Answers Menu Items.
  $qa_answer_uri = 'qa_answer/%qa_answer';
  $qa_answer_uri_argument_position = 1;

  $items[$qa_answer_uri] = array(
    'title callback' => 'entity_label',
    'title arguments' => array('qa_answer', $qa_answer_uri_argument_position),
    'page callback' => 'qa_view',
    'page arguments' => array($qa_answer_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'view',
      'qa_answer',
      $qa_answer_uri_argument_position,
    ),
    'file' => 'qa.pages.inc',
  );

  $items[$qa_answer_uri . '/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items[$qa_answer_uri . '/delete'] = array(
    'title' => 'Delete Answer',
    'title callback' => 'qa_label',
    'title arguments' => array($qa_answer_uri_argument_position),
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'qa_answer_delete_form',
      $qa_answer_uri_argument_position,
    ),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'edit',
      'qa_answer',
      $qa_answer_uri_argument_position,
    ),
    'file' => 'qa.admin.inc',
  );

  $items[$qa_answer_uri . '/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'qa_answer_form',
      $qa_answer_uri_argument_position,
    ),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'edit',
      'qa_answer',
      $qa_answer_uri_argument_position,
    ),
    'file' => 'qa.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  $items['admin/structure/qa-answer-types/%qa_answer_type/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('qa_answer_type_form_delete_confirm', 4),
    'access arguments' => array('administer qa answer types'),
    'weight' => 1,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'qa.admin.inc',
  );

  // Voting Callbacks.
  if (module_exists('votingapi')) {
    $items['qa/vote/answer/%qa_answer/%'] = array(
      'title' => 'Vote',
      'page callback' => 'qa_answer_vote',
      'page arguments' => array(3, 4),
      'access arguments' => array('vote answers'),
      'file' => 'qa.vote.inc',
    );
  }

  // Best Answer Callback.
  $items['qa_best_answer/answer/%qa_answer/%'] = array(
    'title' => 'Best Answer',
    'page callback' => 'qa_best_answer',
    'page arguments' => array(2, 3),
    'access arguments' => array('best answers'),
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function qa_theme() {
  $path = drupal_get_path('module', 'qa') . '/theme';

  $theme = array();
  $theme['qa_section'] = array(
    'render element' => 'elements',
    'template' => 'section',
    'file' => 'theme.inc',
    'path' => $path,
  );
  $theme['qa_question'] = array(
    'render element' => 'elements',
    'template' => 'question',
    'file' => 'theme.inc',
    'path' => $path,
  );
  $theme['qa_answer'] = array(
    'render element' => 'elements',
    'template' => 'answer',
    'file' => 'theme.inc',
    'path' => $path,
  );
  $theme['qa_answer_vote_widget'] = array(
    'variables' => array(
      'answer' => NULL,
      'votes' => NULL,
    ),
    'template' => 'answer_vote_widget',
    'file' => 'theme.inc',
    'path' => $path,
  );
  return $theme;
}

/**
 * Implements hook_votingapi_relationships().
 */
function qa_votingapi_relationships() {
  $relationships = array();
  $relationships[] = array(
    'description' => t('qa answer'),
    'entity_type' => 'qa_answer',
    'base_table' => 'qa_answer',
    'entity_id_column' => 'id',
    'pseudo_vote' => 'votingapi_vote',
    'pseudo_cache' => 'votingapi_cache',
  );
  return $relationships;
}

/**
 * Implements hook_permission().
 */
function qa_permission() {
  $permissions = array(
    // Sections.
    'administer qa sections' => array(
      'title' => t('Administer Sections'),
      'description' => t('Allows users to configure qa question types and their fields.'),
      'restrict access' => TRUE,
    ),
    'create qa sections' => array(
      'title' => t('Create Sections'),
      'description' => t('Allows users to create qas.'),
      'restrict access' => TRUE,
    ),
    'view qa sections' => array(
      'title' => t('View Sections'),
      'description' => t('Allows users to view qas.'),
      'restrict access' => TRUE,
    ),
    'edit any qa sections' => array(
      'title' => t('Edit any Sections'),
      'description' => t('Allows users to edit any qas.'),
      'restrict access' => TRUE,
    ),
    'edit own qa sections' => array(
      'title' => t('Edit own Sections'),
      'description' => t('Allows users to edit own qas.'),
      'restrict access' => TRUE,
    ),
    // Question Types.
    'administer qa question types' => array(
      'title' => t('Administer Questions'),
      'description' => t('Allows users to configure qa question types and their fields.'),
      'restrict access' => TRUE,
    ),
    'create qa entities' => array(
      'title' => t('Create Questions'),
      'description' => t('Allows users to create qas.'),
      'restrict access' => TRUE,
    ),
    'view qa entities' => array(
      'title' => t('View Questions'),
      'description' => t('Allows users to view qas.'),
      'restrict access' => TRUE,
    ),
    'edit any qa entities' => array(
      'title' => t('Edit any Questions'),
      'description' => t('Allows users to edit any qas.'),
      'restrict access' => TRUE,
    ),
    'edit own qa entities' => array(
      'title' => t('Edit own Questions'),
      'description' => t('Allows users to edit own qas.'),
      'restrict access' => TRUE,
    ),
    // Answer Types.
    'administer qa answer types' => array(
      'title' => t('Administer Answers'),
      'description' => t('Allows users to configure qa question types and their fields.'),
      'restrict access' => TRUE,
    ),
    'create qa answers' => array(
      'title' => t('Create Answers'),
      'description' => t('Allows users to create qas.'),
      'restrict access' => TRUE,
    ),
    'view qa answers' => array(
      'title' => t('View Answers'),
      'description' => t('Allows users to view qas.'),
      'restrict access' => TRUE,
    ),
    'edit any qa answers' => array(
      'title' => t('Edit any Answers'),
      'description' => t('Allows users to edit any qas.'),
      'restrict access' => TRUE,
    ),
    'edit own qa answers' => array(
      'title' => t('Edit own Answers'),
      'description' => t('Allows users to edit own qas.'),
      'restrict access' => TRUE,
    ),
    // Vote Permissions.
    'vote answers' => array(
      'title' => t('Vote on Answers'),
      'description' => t('Allows users to vote on answers.'),
    ),
    // Best Answers.
    'best answers' => array(
      'title' => t('Choose Best Answer'),
      'description' => t('Allows user to choose the Best Answer'),
    ),
  );

  return $permissions;
}

/**
 * Access callback for Sections.
 */
function qa_section_access($op, $section, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }
  switch ($op) {
    case 'create':
      return user_access('administer qa sections', $account) || user_access('create qa sections', $account);

    case 'view':
      return user_access('administer qa sections', $account) || user_access('view qa sections', $account);

    case 'edit':
      return user_access('administer qa sections') || user_access('edit any qa sections') || (user_access('edit own qa sections') && ($section->uid == $account->uid));
  }
}

/**
 * Load a single Section.
 *
 * @param int $id
 *   Section id.
 * @param boolean $reset
 *   Reset.
 *
 * @return object
 *   Section Entity.
 */
function qa_section_load($id, $reset = FALSE) {
  $section = qa_section_load_multiple(array($id), array(), $reset);
  return reset($section);
}

/**
 * Load multiple Sections.
 *
 * @param array $ids
 *   An array of section ids.
 * @param array $conditions
 *   Conditions for loading multple.
 * @param bool $reset
 *   Reset.
 *
 * @return object
 *   Section entity.
 */
function qa_section_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('qa_section', $ids, $conditions, $reset);
}

/**
 * Save a Section.
 *
 * @param object $section
 *   Section entity.
 */
function qa_section_save($section) {
  entity_save('qa_section', $section);
}

/**
 * Delete a single Section.
 *
 * @param object $section
 *   Section entity.
 */
function qa_section_delete($section) {
  entity_delete('qa_section', entity_id('qa_section', $section));
}

/**
 * Delete multiple Sections.
 *
 * @param array $ids
 *   Array of section ids.
 */
function qa_section_delete_multiple($ids) {
  entity_delete_multiple('qa_section', $ids);
}

/**
 * Access callback for Questions.
 */
function qa_access($op, $qa, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }
  switch ($op) {
    case 'create':
      return user_access('administer qa entities', $account) || user_access('create qa entities', $account);

    case 'view':
      return user_access('administer qa entities', $account) || user_access('view qa entities', $account);

    case 'edit':
      return user_access('administer qa entities') || user_access('edit any qa entities') || (user_access('edit own qa entities') && ($qa->uid == $account->uid));
  }
}

/**
 * Load a Question.
 *
 * @param int $id
 *   Question entity.
 * @param boolean $reset
 *   Reset.
 *
 * @return object
 *   Question entity.
 */
function qa_question_load($id, $reset = FALSE) {
  $question = qa_question_load_multiple(array($id), array(), $reset);
  return reset($question);
}

/**
 * Load multiple Questions.
 *
 * @param array $ids
 *   An array of question ids.
 * @param array $conditions
 *   Conditions for loading questions.
 * @param boolean $reset
 *   Reset.
 *
 * @return object
 *   Question entity.
 */
function qa_question_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('qa_question', $ids, $conditions, $reset);
}

/**
 * Lists Questions based on Section id.
 *
 * @param int $id
 *   Question id.
 * @param string $view_mode
 *   The way we want section rendered full, teaser, etc.
 *
 * @return string
 *   Html list of questions.
 */
function qa_question_list($id, $view_mode = 'teaser') {

  module_load_include('inc', 'qa', 'qa.pages');

  $limit = variable_get('section_count', 5);
  $query = db_select('qa_question', 'q')->extend('PagerDefault')->limit($limit)->condition('q.sid', $id, '=')->fields('q', array('id'));

  $result = $query->execute()->fetchAll();

  $questions = '';
  if (count($result) == 0) {
    $questions = t('This section does not have any questions associated with it.');
  }
  else {
    foreach ($result as $obj) {
      $question = qa_question_load($obj->id);
      $questions .= render(qa_question_view($question, $view_mode));;
    }
    $questions .= theme('pager', array('tags' => array()));
  }

  return $questions;
}

/**
 * Save a Question.
 *
 * @param object $question
 *   Question entity.
 */
function qa_question_save($question) {
  entity_save('qa_question', $question);
}

/**
 * Delete single Question.
 *
 * @param object $question
 *   Question entity.
 */
function qa_question_delete($question) {
  entity_delete('qa_question', entity_id('qa_question', $question));
}

/**
 * Delete multiple Questions.
 *
 * @param array $ids
 *   An array of question ids.
 */
function qa_question_delete_multiple($ids) {
  entity_delete_multiple('qa_question', $ids);
}

/**
 * Access callback for Question Types.
 */
function qa_question_type_access($op, $entity = NULL) {
  return user_access('administer qa question types');
}

/**
 * Load a Question Type.
 *
 * @param string $type_name
 *   Question type name.
 *
 * @return object
 *   Question type entity.
 */
function qa_question_type_load($type_name) {
  return qa_question_types($type_name);
}

/**
 * List of Question Types.
 *
 * @param string $type_name
 *   Name of the Question type entity.
 *
 * @return array
 *   An array of question types.
 */
function qa_question_types($type_name = NULL) {
  $types = entity_load_multiple_by_name('qa_question_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Save Question Type.
 *
 * @param object $question_type
 *   Question type entity.
 */
function qa_question_type_save($question_type) {
  entity_save('qa_question_type', $question_type);
}

/**
 * Delete single Question Type.
 *
 * @param object $question_type
 *   Question type entity.
 */
function qa_question_type_delete($question_type) {
  entity_delete('qa_question_type', entity_id('qa_question_type', $question_type));
}

/**
 * Delete multiple Question Types.
 *
 * @param array $ids
 *   An array of question type ids.
 */
function qa_question_type_delete_multiple($ids) {
  entity_delete_multiple('qa_question_type', $ids);
}

/**
 * Access callback for Answers.
 */
function qa_answer_access($op, $answer, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }
  switch ($op) {
    case 'create':
      return user_access('administer qa answers', $account) || user_access('create qa answers', $account);

    case 'view':
      return user_access('administer qa answers', $account) || user_access('view qa answers', $account);

    case 'edit':
      return user_access('administer qa answers') || user_access('edit any qa answers') || (user_access('edit own qa answers') && ($answer->uid == $account->uid));
  }
}

/**
 * Load an Answer.
 *
 * @param int $id
 *   Answer entity id.
 * @param boolean $reset
 *   Reset.
 *
 * @return object
 *   Answer entity.
 */
function qa_answer_load($id, $reset = FALSE) {
  $answer = qa_answer_load_multiple(array($id), array(), $reset);
  return reset($answer);
}

/**
 * Load multiple Answers based on certain conditions.
 *
 * @param array $ids
 *   An array of answer ids.
 * @param array $conditions
 *   An array of conditions for loading answer entities.
 * @param boolean $reset
 *   Reset.
 *
 * @return object
 *   An array of Answer entities.
 */
function qa_answer_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('qa_answer', $ids, $conditions, $reset);
}

/**
 * Lists Answers based on Question.id.
 *
 * If votingapi is enabled, answers are ordered by most voted.
 *
 * @param int $id
 *   Question entity id.
 * @param string $view_mode
 *   The way we want section rendered full, teaser, etc.
 *
 * @return streng
 *   Html list of answers associated with this question.
 */
function qa_answers_list($id, $view_mode = 'teaser') {

  global $user;
  module_load_include('inc', 'qa', 'qa.vote');
  module_load_include('inc', 'qa', 'qa.pages');

  $limit = variable_get('answer_count', 5);
  $query = db_select('qa_answer', 'a')->extend('PagerDefault')->limit($limit)->condition('a.qid', $id, '=');

  // Order by Best Answer.
  $query->orderBy('a.best_answer', 'DESC');

  // Order by votes if votingapi enabled.
  if (module_exists('votingapi')) {
    $query->leftJoin('votingapi_cache', 'v', 'a.id = v.entity_id');

    // Show sums and null votes.
    $db_or = db_or();
    $db_or->condition('v.function', 'sum', '=');
    $db_or->condition('v.function', NULL);
    $query->condition($db_or);

    $query->orderBy('v.value', 'DESC');
  }

  $query->fields('a', array('id'));
  $result = $query->execute()->fetchAll();

  $answers = '';
  if (count($result) == 0) {
    $answers = t('This question does not have any answers.');
  }
  else {
    foreach ($result as $obj) {
      $answer = qa_answer_load($obj->id);
      $answers .= render(qa_answer_view($answer, $view_mode));
    }
  }

  $answers .= theme('pager', array('tags' => array()));
  return $answers;
}

/**
 * Save an Answer.
 *
 * @param object $answer
 *   Answer entity.
 */
function qa_answer_save($answer) {
  entity_save('qa_answer', $answer);
}

/**
 * Delete a single Answer.
 *
 * @param object $answer
 *   Answer entity.
 */
function qa_answer_delete($answer) {
  entity_delete('qa_answer', entity_id('qa_answer', $answer));
}

/**
 * Delete multiple Answers.
 *
 * @param array $ids
 *   An array of answer ids.
 */
function qa_answer_delete_multiple($ids) {
  entity_delete_multiple('qa_answer', $ids);
}

/**
 * Access callback for Answer Types.
 */
function qa_answer_type_access($op, $qa, $account = NULL, $entity_type = NULL) {
  return user_access('administer qa question types');
}

/**
 * Load an Answer Type.
 *
 * @param string $answer_type
 *   Answer type entity name.
 *
 * @return object
 *   Answer type entity.
 */
function qa_answer_type_load($answer_type) {
  return qa_answer_types($answer_type);
}

/**
 * List of Answer Types.
 *
 * @param string $type_name
 *   Answer type entity name.
 *
 * @return object
 *   An array of answer type entities.
 */
function qa_answer_types($type_name = NULL) {
  $types = entity_load_multiple_by_name('qa_answer_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Save an Answer Type.
 *
 * @param object $answer_type
 *   Answer type object.
 */
function qa_answer_type_save($answer_type) {
  entity_save('qa_answer_type', $answer_type);
}

/**
 * Delete a single Answer Type.
 *
 * @param object $answer_type
 *   Answer type object.
 */
function qa_answer_type_delete($answer_type) {
  entity_delete('qa_answer_type', entity_id('qa_answer_type', $answer_type));
}

/**
 * Delete multiple Answer Types.
 *
 * @param array $ids
 *   An array of answer type ids.
 */
function qa_answer_type_delete_multiple($ids) {
  entity_delete_multiple('qa_answer_type', $ids);
}

/**
 * Implements hook_pathauto().
 */
function qa_pathauto($op) {
  $settings = array();
  $settings['module'] = 'qa_question';
  $settings['token_type'] = 'qa_question';
  $settings['groupheader'] = t('QA Question paths');
  $settings['patterndescr'] = t('Default path pattern (applies to all content types with blank patterns below)');
  $settings['patterndefault'] = 'question/[qa_question:title]';
  //$settings['batch_update_callback'] = 'node_pathauto_bulk_update_batch_process';

  foreach (qa_question_types() as $type => $info) {
    $settings['patternitems'][$type] = t('Pattern for all @question paths', array('@question' => $info->label));
  }
  return (object) $settings;
}

/**
 * Helper function to create alias for contests.
 */
function qa_create_alias($question, $op) {
  module_load_include('inc', 'pathauto');
  pathauto_create_alias('qa_question', $op, 'qa_question/' . $question->id, array('qa_question' => $question), 'qa_question');
}

/**
 * Implements hook_views_api().
 */
function qa_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_views_default_views().
 */
function qa_views_default_views() {
  $files = file_scan_directory(drupal_get_path('module', 'qa') . '/views', '/.inc$/');
  $views = array();
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}

/**
 * Implements hook_data_alter().
 */
function qa_views_data_alter(&$data) {
  // Sections
  $data['views_entity_qa_section']['view_qa_section'] = array(
    'field' => array(
      'title' => t('View link'),
      'help' => t('Provide a simple link to view the QA Section, if this is possible.'),
      'handler' => 'views_handler_field_qa_section_link',
      'real field' => 'url',
      'type' => 'uri',
    ),
  );
  $data['views_entity_qa_section']['edit_qa_section'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the QA Section, if this is possible.'),
      'handler' => 'views_handler_field_qa_section_link_edit',
      'real field' => 'url',
      'type' => 'uri',
    ),
  );
  $data['views_entity_qa_section']['delete_qa_section'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the QA Section, if this is possible.'),
      'handler' => 'views_handler_field_qa_section_link_delete',
      'real field' => 'url',
      'type' => 'uri',
    ),
  );
  // Question
  $data['views_entity_qa_question']['view_qa_question'] = array(
    'field' => array(
      'title' => t('View link'),
      'help' => t('Provide a simple link to view the QA Question, if this is possible.'),
      'handler' => 'views_handler_field_qa_question_link',
      'real field' => 'url',
      'type' => 'uri',
    ),
  );
  $data['views_entity_qa_question']['edit_qa_question'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the QA Question, if this is possible.'),
      'handler' => 'views_handler_field_qa_question_link_edit',
      'real field' => 'url',
      'type' => 'uri',
    ),
  );
  $data['views_entity_qa_question']['delete_qa_question'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the QA Question, if this is possible.'),
      'handler' => 'views_handler_field_qa_question_link_delete',
      'real field' => 'url',
      'type' => 'uri',
    ),
  );
  // Answers
  $data['views_entity_qa_answer']['view_qa_answer'] = array(
    'field' => array(
      'title' => t('View link'),
      'help' => t('Provide a simple link to view the QA Answer, if this is possible.'),
      'handler' => 'views_handler_field_qa_answer_link',
      'real field' => 'url',
      'type' => 'uri',
    ),
  );
  $data['views_entity_qa_answer']['edit_qa_answer'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the QA Answer, if this is possible.'),
      'handler' => 'views_handler_field_qa_answer_link_edit',
      'real field' => 'url',
      'type' => 'uri',
    ),
  );
  $data['views_entity_qa_answer']['delete_qa_answer'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the QA Answer, if this is possible.'),
      'handler' => 'views_handler_field_qa_answer_link_delete',
      'real field' => 'url',
      'type' => 'uri',
    ),
  );
}

/**
 * Implements hook_ctools_plugin_api().
 */
function qa_ctools_plugin_api($module, $api) {
  if ($module == 'page_manager' && $api == 'pages_default') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_default_page_manager_pages().
 */
function qa_default_page_manager_pages() {
  $files = file_scan_directory(drupal_get_path('module', 'qa') . '/panels/pages', '/.inc$/');
  $pages = array();
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($page)) {
      $pages[$page->name] = $page;
    }
  }
  return $pages;
}

/**
 * Checks to see if a user has a given role.
 *
 * @param object $user
 *   User entity.
 * @param array $roles
 *   Roles to be checked.
 *
 * @return boolean
 *   Whether or not the user has one of the requested roles
 */
function qa_role_check($user, $roles) {
  foreach ($user->roles as $role => $value) {
    if (in_array($role, $roles)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Callback that allows user to choose the best Answer for a Question.
 *
 * @param object $answer
 *   Answer entity.
 * @param string $op
 *   The operation being performed.
 */
function qa_best_answer($answer, $op) {

  $question = qa_question_load($answer->qid);
  switch ($op) {
    case 'add':
      $question->best_answer = 1;
      $answer->best_answer = 1;
      break;

    case 'remove':
      $question->best_answer = 0;
      $answer->best_answer = 0;
      break;
  }
  $question->save();
  $answer->save();
  $path = $question->uri();
  drupal_goto($path['path']);
}

