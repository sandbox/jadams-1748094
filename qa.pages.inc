<?php

/**
 * @file
 * Contiains Questions & Answers page view functions.
 * Its important to not that if this module is to be used with panels these
 * fuctions will not run.
 */

/**
 * View Sections.
 *
 * @param object $qa_section
 *   The section entity.
 * @param string $view_mode
 *   String indicating the mode to render section.
 *
 * @return object
 *   The rendered section entity.
 */
function qa_section_view($qa_section, $view_mode = 'full') {
  // drupal_set_title(entity_label('qa_section', $qa));
  return entity_view('qa_section', array(entity_id('qa_section', $qa_section) => $qa_section), 'full');
}

/**
 * View Questions.
 *
 * @param object $qa_question
 *   The question entity.
 * @param string $view_mode
 *   String indicating the mode to render question.
 *
 * @return object
 *   The rendered section entity.
 */
function qa_question_view($qa_question, $view_mode = 'full') {
  // drupal_set_title(entity_label('qa_question', $qa));
  return entity_view('qa_question', array(entity_id('qa_question', $qa_question) => $qa_question), $view_mode);
}

/**
 * View Answers.
 *
 * @param object $qa_answer
 *   The answer entity.
 * @param string $view_mode
 *   String indicating the mode to render answer.
 *
 * @return object
 *   The rendered section entity.
 */
function qa_answer_view($qa_answer, $view_mode = 'full') {
  // drupal_set_title(entity_label('qa_question', $qa));
  return entity_view('qa_answer', array(entity_id('qa_answer', $qa_answer) => $qa_answer), $view_mode);
}

