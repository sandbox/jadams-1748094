<?php

/**
 * @file
 * Field handler to present a link for qa entities.
 *
 * @ingroup views_field_handlers
 */

// Sections
class views_handler_field_qa_section_link extends views_handler_field_entity {
  function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
    parent::options_form($form, $form_state);

    // The path is set by render_link function so don't allow to set it.
    $form['alter']['path'] = array('#access' => FALSE);
    $form['alter']['external'] = array('#access' => FALSE);
  }

  function render($values) {
    if ($entity = $this->get_value($values)) {
      return $this->render_link($entity, $values);
    }
  }

  function render_link($qa_section, $values) {
    if (qa_section_access('view', $qa_section)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "qa_section/$qa_section->sid";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('View');
      return $text;
    }
  }
}

class views_handler_field_qa_section_link_edit extends views_handler_field_qa_section_link {
  function render_link($qa_section, $values) {
    if (qa_section_access('edit', $qa_section)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "qa_section/$qa_section->id/edit";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Edit');
      return $text;
    }
  }
}

class views_handler_field_qa_section_link_delete extends views_handler_field_qa_section_link {
  function render_link($qa_section, $values) {
    if (qa_section_access('edit', $qa_section)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "qa_section/$qa_section->id/delete";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Delete');
      return $text;
    }
  }
}

class views_handler_field_qa_question_link extends views_handler_field_qa_section_link {
  function render_link($qa_question, $values) {
    if (qa_access('view', $qa_question)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "qa_question/$qa_question->id";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('View');
      return $text;
    }
  }
}

class views_handler_field_qa_question_link_edit extends views_handler_field_qa_section_link {
  function render_link($qa_question, $values) {
    if (qa_access('edit', $qa_question)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "qa_question/$qa_question->id/edit";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Edit');
      return $text;
    }
  }
}

class views_handler_field_qa_question_link_delete extends views_handler_field_qa_section_link {
  function render_link($qa_question, $values) {
    if (qa_access('edit', $qa_question)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "qa_question/$qa_question->id/delete";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Delete');
      return $text;
    }
  }
}

class views_handler_field_qa_answer_link extends views_handler_field_qa_section_link {
  function render_link($qa_answer, $values) {
    if (qa_access('view', $qa_answer)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "qa_answer/$qa_answer->id";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('View');
      return $text;
    }
  }
}

class views_handler_field_qa_answer_link_edit extends views_handler_field_qa_section_link {
  function render_link($qa_answer, $values) {
    if (qa_access('edit', $qa_answer)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "qa_answer/$qa_answer->id/edit";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Edit');
      return $text;
    }
  }
}

class views_handler_field_qa_answer_link_delete extends views_handler_field_qa_section_link {
  function render_link($qa_answer, $values) {
    if (qa_access('edit', $qa_answer)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "qa_answer/$qa_answer->id/delete";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Delete');
      return $text;
    }
  }
}

