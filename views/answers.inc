<?php
$view = new view();
$view->name = 'answers';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'qa_answer';
$view->human_name = 'Answers';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Answers';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'entity';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'This question has not been answered yet.';
$handler->display->display_options['empty']['area']['format'] = 'full_html';
/* Relationship: QA Answer: Vote results */
$handler->display->display_options['relationships']['votingapi_cache']['id'] = 'votingapi_cache';
$handler->display->display_options['relationships']['votingapi_cache']['table'] = 'qa_answer';
$handler->display->display_options['relationships']['votingapi_cache']['field'] = 'votingapi_cache';
$handler->display->display_options['relationships']['votingapi_cache']['votingapi'] = array(
  'value_type' => 'points',
  'tag' => 'vote',
  'function' => 'sum',
);
/* Field: QA Answer: Qa answer ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'qa_answer';
$handler->display->display_options['fields']['id']['field'] = 'id';
/* Sort criterion: QA Answer: Best_answer */
$handler->display->display_options['sorts']['best_answer']['id'] = 'best_answer';
$handler->display->display_options['sorts']['best_answer']['table'] = 'qa_answer';
$handler->display->display_options['sorts']['best_answer']['field'] = 'best_answer';
$handler->display->display_options['sorts']['best_answer']['order'] = 'DESC';
/* Sort criterion: Vote results: Value */
$handler->display->display_options['sorts']['value']['id'] = 'value';
$handler->display->display_options['sorts']['value']['table'] = 'votingapi_cache';
$handler->display->display_options['sorts']['value']['field'] = 'value';
$handler->display->display_options['sorts']['value']['relationship'] = 'votingapi_cache';
$handler->display->display_options['sorts']['value']['order'] = 'DESC';
$handler->display->display_options['sorts']['value']['coalesce'] = 0;
/* Contextual filter: QA Answer: QA Question */
$handler->display->display_options['arguments']['qid']['id'] = 'qid';
$handler->display->display_options['arguments']['qid']['table'] = 'qa_answer';
$handler->display->display_options['arguments']['qid']['field'] = 'qid';
$handler->display->display_options['arguments']['qid']['default_action'] = 'default';
$handler->display->display_options['arguments']['qid']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['qid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['qid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['qid']['summary_options']['items_per_page'] = '25';

