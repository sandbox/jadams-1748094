<?php

/**
 * @file
 * Sections View.
 */

$view = new view();
$view->name = 'qa_sections';
$view->description = 'List of Section';
$view->tag = 'default';
$view->base_table = 'qa_section';
$view->human_name = 'Sections';
$view->core = 7;
$view->api_version = '3.0';
/* Edit this to true to make a default view disabled initially */
$view->disabled = FALSE;

/* Display: Master */

$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Sections';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer qa sections';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'entity';
$handler->display->display_options['row_options']['view_mode'] = 'teaser';
/* Relationship: QA Section: Author */

$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'qa_section';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
/* Field: QA Section: Qa section ID */

$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'qa_section';
$handler->display->display_options['fields']['id']['field'] = 'id';
/* Field: QA Section: Date created */

$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'qa_section';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = 'Created';
$handler->display->display_options['fields']['created']['date_format'] = 'short';
/* Field: QA Section: Title */

$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'qa_section';
$handler->display->display_options['fields']['title']['field'] = 'title';
/* Field: QA Section: Delete link */

$handler->display->display_options['fields']['delete_qa_section']['id'] = 'delete_qa_section';
$handler->display->display_options['fields']['delete_qa_section']['table'] = 'views_entity_qa_section';
$handler->display->display_options['fields']['delete_qa_section']['field'] = 'delete_qa_section';
$handler->display->display_options['fields']['delete_qa_section']['label'] = 'Delete';
/* Field: QA Section: Edit link */

$handler->display->display_options['fields']['edit_qa_section']['id'] = 'edit_qa_section';
$handler->display->display_options['fields']['edit_qa_section']['table'] = 'views_entity_qa_section';
$handler->display->display_options['fields']['edit_qa_section']['field'] = 'edit_qa_section';
$handler->display->display_options['fields']['edit_qa_section']['label'] = 'Edit';
/* Field: QA Section: URL */

$handler->display->display_options['fields']['url']['id'] = 'url';
$handler->display->display_options['fields']['url']['table'] = 'views_entity_qa_section';
$handler->display->display_options['fields']['url']['field'] = 'url';
$handler->display->display_options['fields']['url']['label'] = '';
$handler->display->display_options['fields']['url']['exclude'] = TRUE;
$handler->display->display_options['fields']['url']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['url']['display_as_link'] = FALSE;
$handler->display->display_options['fields']['url']['link_to_entity'] = 0;

/* Display: Admin List */

$handler = $view->new_display('page', 'Admin List', 'page_1');
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'id' => 'id',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: QA Section: URL */

$handler->display->display_options['fields']['url']['id'] = 'url';
$handler->display->display_options['fields']['url']['table'] = 'views_entity_qa_section';
$handler->display->display_options['fields']['url']['field'] = 'url';
$handler->display->display_options['fields']['url']['label'] = '';
$handler->display->display_options['fields']['url']['exclude'] = TRUE;
$handler->display->display_options['fields']['url']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['url']['display_as_link'] = FALSE;
$handler->display->display_options['fields']['url']['link_to_entity'] = 0;
/* Field: QA Section: Title */

$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'qa_section';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['path'] = '[url]';
/* Field: QA Section: Date created */

$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'qa_section';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = 'Created';
$handler->display->display_options['fields']['created']['date_format'] = 'short';
/* Field: QA Section: Edit link */

$handler->display->display_options['fields']['edit_qa_section']['id'] = 'edit_qa_section';
$handler->display->display_options['fields']['edit_qa_section']['table'] = 'views_entity_qa_section';
$handler->display->display_options['fields']['edit_qa_section']['field'] = 'edit_qa_section';
$handler->display->display_options['fields']['edit_qa_section']['label'] = 'Edit';
/* Field: QA Section: Delete link */

$handler->display->display_options['fields']['delete_qa_section']['id'] = 'delete_qa_section';
$handler->display->display_options['fields']['delete_qa_section']['table'] = 'views_entity_qa_section';
$handler->display->display_options['fields']['delete_qa_section']['field'] = 'delete_qa_section';
$handler->display->display_options['fields']['delete_qa_section']['label'] = 'Delete';
/* Field: User: Name */

$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'Author';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: QA Section: Title */

$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'qa_section';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'contains';
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
$handler->display->display_options['path'] = 'admin/content/qa/sections';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'QA Sections';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;

