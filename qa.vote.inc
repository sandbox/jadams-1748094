<?php

/**
 * @file
 * Contiains Questions & Answers Voting Functions
 */

/**
 * Ajax Callback that allows users to vote on an Answers.
 *
 * @param object $answer
 *   The answer entity.
 */
function qa_answer_vote($answer, $op) {

  switch ($op) {
    case 'up':
      $value = 1;
      break;

    case 'down':
      $value = -1;
      break;
  }

  // Cast vote on answer.
  $votes = array();
  $votes[] = array(
    'entity_type' => 'qa_answer',
    'entity_id' => $answer->id,
    'value_type' => 'points',
    'tag' => 'qa_answer_votes',
    'value' => $value,
  );
  votingapi_set_votes($votes);

  // Update Result.
  $result = qa_answer_get_votes($answer);
  $result == 1 ? $result = $result . ' Vote' : $result = $result . ' Votes';

  // Rebuild display.
  $updated_votes = '<div class="answer-votes">' . $result . '</div>';

  $commands = array();
  $commands[] = ajax_command_replace('#qa-answer-' . $answer->id . ' .answer-votes', $updated_votes);
  $page = array('#type' => 'ajax', '#commands' => $commands);
  ajax_deliver($page);
}

/**
 * Get the number of votes per Answer.
 *
 * @param object $answer
 *   The answer entity.
 *
 * @return int
 *   The number of votes associated with the answer object.
 */
function qa_answer_get_votes($answer) {
  $criteria = array(
    'entity_type' => 'qa_answer',
    'entity_id' => $answer->id,
    'value_type' => 'points',
    'tag' => 'qa_answer_votes',
    'function' => 'sum',
  );
  $result = votingapi_select_single_result_value($criteria);
  if (!$result) {
    $result = 0;
  }
  return $result;
}

/**
 * Check to see if a user has voted on an answer inside of a question.
 *
 * @param int $qid
 *   Unique id for the question entity.
 * @param int $uid
 *   Unique id for the user.
 *
 * @return boolean
 *   Boolean indicating whether or not this answer has already been voted on by
 *   this user.
 */
function qa_check_user_vote($qid, $uid) {

  $query = db_select('qa_answer', 'a');
  $query->join('votingapi_vote', 'v', 'a.id = v.entity_id');
  $query->condition('a.qid', $qid, '=')->condition('v.uid', $uid, '=');
  $query->fields('a', array('id'));
  $result = $query->execute()->fetchAll();

  if (count($result) >= variable_get('vote_count', 1)) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

