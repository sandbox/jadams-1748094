<?php

/**
 * @file
 * Forms and admin pages for Questions & Answers.
 */

/**
 * Qa Section Add / Edit Form.
 *
 * @param object $section
 *   The section entity.
 */
function qa_section_form($form, &$form_state, $section) {
  $form_state['qa_section'] = $section;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Title'),
    '#default_value' => $section->title,
  );

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 99,
  );

  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Question / Answer Types'),
    '#group' => 'additional_settings',
  );

  $question_types = array();
  foreach (qa_question_types() as $type) {
    $question_types[$type->type] = $type->label;
  }
  $form['types']['question_type'] = array(
    '#type' => 'select',
    '#title' => t('Question Type'),
    '#options' => $question_types,
    '#default_value' => $section->question_type,
  );

  $answer_types = array();
  foreach (qa_answer_types() as $type) {
    $answer_types[$type->type] = $type->label;
  }
  $form['types']['answer_type'] = array(
    '#type' => 'select',
    '#title' => t('Answer Type'),
    '#options' => $answer_types,
    '#default_value' => $section->answer_type,
  );

  $form['role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role Access'),
    '#group' => 'additional_settings',
  );

  $form['role']['ask_roles'] = array(
    '#type' => 'select',
    '#title' => t('Roles that can ask questions in this section'),
    '#options' => user_roles(),
    '#multiple' => TRUE,
    '#size' => 4,
    '#default_value' => $section->ask_roles,
    '#required' => TRUE,
  );

  $form['role']['answer_roles'] = array(
    '#type' => 'select',
    '#title' => t('Roles that can answer questions in this section'),
    '#options' => user_roles(),
    '#multiple' => TRUE,
    '#size' => 4,
    '#default_value' => $section->answer_roles,
    '#required' => TRUE,
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#group' => 'additional_settings',
  );

  $form['settings']['active'] = array(
    '#type' => 'radios',
    '#title' => t('Active'),
    '#default_value' => $section->active,
    '#options' => array(0 => t('Closed'), 1 => t('Active')),
    '#description' => t('Determines whether or not this section is open for new questions.'),
  );

  $form['settings']['question_teaser'] = array(
    '#type' => 'radios',
    '#title' => t('Show Question List in Teaser'),
    '#default_value' => $section->question_teaser,
    '#options' => array(0 => t('No'), 1 => t('Yes')),
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $section->uid,
  );

  field_attach_form('qa_section', $section, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  // If path is enabled show the path settings.
  if (module_exists('path')) {
    qa_path_settings('qa_section', $section, $form);
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Section'),
    '#submit' => $submit + array('qa_section_form_submit'),
  );

  $id = entity_id('qa_section', $section);
  if (!empty($id) && qa_access('edit', $section)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('qa_section_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Qa Section Add / Edit Form Validate.
 */
function qa_section_form_submit($form, &$form_state) {
  $section = $form_state['qa_section'];
  entity_form_submit_build_entity('qa_section', $section, $form, $form_state);
  qa_section_save($section);

  if (module_exists('path')) {
    qa_path_save('qa_section', $section);
  }

  $section_uri = entity_uri('qa_section', $section);
  $form_state['redirect'] = $section_uri['path'];
  drupal_set_message(t('Section %title saved.', array('%title' => entity_label('qa_section', $section))));
}

/**
 * Qa Section Delete Confirmation Form.
 *
 * @param object $section
 *   The section entity.
 */
function qa_section_delete_form($form, &$form_state, $section) {
  $form_state['qa_section'] = $section;
  $qa_answer_uri = entity_uri('qa_section', $section);
  return confirm_form($form,
    t('Are you sure you want to delete qa %title?', array('%title' => entity_label('qa_section', $section))),
    $qa_answer_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Qa Section Delete Confirmation Form Submit.
 */
function qa_section_delete_form_submit($form, &$form_state) {
  $section = $form_state['qa_section'];
  qa_section_delete($section);
  drupal_set_message(t('Question %title deleted.', array('%title' => entity_label('qa_section', $section))));
  $form_state['redirect'] = '<front>';
}

/**
 * Qa Section Delete Redirect.
 */
function qa_section_form_submit_delete($form, &$form_state) {
  $section = $form_state['qa_section'];
  $section_uri = entity_uri('qa_section', $section);
  $form_state['redirect'] = $section_uri['path'] . '/delete';
}

/**
 * Generates the qa question type editing form.
 *
 * @param object $qa_question_type
 *   The question type entity.
 * @param string $op
 *   The operation being performed add, edit, delete, etc..
 */
function qa_question_type_form($form, &$form_state, $qa_question_type, $op = 'edit') {

  if ($op == 'clone') {
    $qa_question_type->label .= ' (cloned)';
    $qa_question_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $qa_question_type->label,
    '#description' => t('The human-readable name of this qa question type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($qa_question_type->type) ? $qa_question_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $qa_question_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'qa_question_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this qa question type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($qa_question_type->description) ? $qa_question_type->description : '',
    '#description' => t('Description about the qa question type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save qa question type'),
    '#weight' => 40,
  );

  if (!$qa_question_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete qa question type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('qa_question_type_form_submit_delete'),
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing qa_question_type.
 */
function qa_question_type_form_submit(&$form, &$form_state) {
  $qa_question_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  qa_question_type_save($qa_question_type);

  // Redirect user back to list of qa question types.
  $form_state['redirect'] = 'admin/structure/qa-question-types';
}

/**
 * Question type delete redirect.
 */
function qa_question_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/qa-question-types/' . $form_state['qa_question_type']->type . '/delete';
}

/**
 * Question type delete form.
 */
function qa_question_type_form_delete_confirm($form, &$form_state, $qa_question_type) {
  $form_state['qa_question_type'] = $qa_question_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['qa_question_type_id'] = array(
    '#type' => 'value',
    '#value' => entity_id('qa_question_type', $qa_question_type),
  );

  return confirm_form($form,
    t('Are you sure you want to delete qa question type %title?', array('%title' => entity_label('qa_question_type', $qa_question_type))),
    'qa_question/' . entity_id('qa_question_type', $qa_question_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Question type delete form submit handler.
 */
function qa_question_type_form_delete_confirm_submit($form, &$form_state) {
  $qa_question_type = $form_state['qa_question_type'];
  qa_question_type_delete($qa_question_type);

  watchdog('qa_question_type', '@type: deleted %title.', array('@type' => $qa_question_type->type, '%title' => $qa_question_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $qa_question_type->type, '%title' => $qa_question_type->label)));

  $form_state['redirect'] = 'admin/structure/qa-question-types';
}

/**
 * Page to select Question type to add new Question.
 */
function qa_question_admin_add_page() {
  $items = array();
  foreach (qa_question_types() as $qa_question_type_key => $qa_question_type) {
    $items[] = l(entity_label('qa_question_type', $qa_question_type), 'qa_question/add/' . $qa_question_type_key);
  }

  return array(
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => t('Select type of Quesiton to create.'),
    ),
  );
}

/**
 * Page to select Answer type to add new Answer.
 */
function qa_answer_admin_add_page() {
  $items = array();
  foreach (qa_answer_types() as $qa_answer_type_key => $qa_answer_type) {
    $items[] = l(entity_label('qa_answer_type', $qa_answer_type), 'qa_answer/add/' . $qa_answer_type_key);
  }

  return array(
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => t('Select type of Answer to create.'),
    ),
  );
}

/**
 * Add new Question page callback.
 */
function qa_add($type) {
  $qa_question_type = qa_question_types($type);

  $qa = entity_create('qa_question', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('qa_question_type', $qa_question_type))));

  $output = drupal_get_form('qa_question_form', $qa, 'add');

  return $output;
}

/**
 * Mini Question form used on Sections.
 *
 * @param object $section
 *   The section entity.
 */
function qa_question_mini_form($form, &$form_state, $section) {

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Ask a Question about @section', array('@section' => $section->title)),
  );

  $form['section'] = array(
    '#type' => 'value',
    '#value' => $section,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Ask Question'),
  );

  return $form;
}

/**
 * Submit handler for Mini Question Form.
 *
 * Creates Question Entity and redirect to Edit Form.
 */
function qa_question_mini_form_submit($form, &$form_state) {

  // Create Question.
  $section = $form_state['values']['section'];
  $question = entity_create('qa_question', array(
      'title' => $form_state['values']['title'],
      'type' => $section->question_type,
      'sid' => $section->id,
    )
  );
  $question->save();

  // Create an alias automatically.
  if (module_exists('pathauto') && !empty($form_state['values']['path']['pathauto'])) {
    qa_create_alias($question, 'insert');
  }

  // Redirect to Question Edit Form.
  $path = $question->uri();
  $form_state['redirect'] = $path['path'] . '/edit';
}

/**
 * Question Form.
 */
function qa_question_form($form, &$form_state, $question, $op = 'edit') {

  $form_state['op'] = $op;
  drupal_set_title($question->title);

  $form_state['qa_question'] = $question;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Title'),
    '#default_value' => $question->title,
  );

  $form['active'] = array(
    '#type' => 'radios',
    '#title' => t('Active'),
    '#default_value' => $question->active,
    '#options' => array(0 => t('Closed'), 1 => t('Active')),
    '#description' => t('Determines whether or not this question is open for new answers.'),
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $question->uid,
  );

  field_attach_form('qa_question', $question, $form, $form_state);

  // If path is enabled show the path settings.
  if (module_exists('path')) {
    qa_path_settings('qa_question', $question, $form);
  }

  if (module_exists('pathauto')) {
    pathauto_field_attach_form('qa_question', $question, $form, $form_state, LANGUAGE_NONE);
  }

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Ask Question'),
    '#submit' => $submit + array('qa_question_form_submit'),
  );

  // Show Delete button if we edit qa.
  $id = entity_id('qa_question', $question);
  if (!empty($id) && qa_access('edit', $question)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('qa_question_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Question submit handler.
 */
function qa_question_form_submit($form, &$form_state) {
  $qa = $form_state['qa_question'];
  $op = $form_state['op'];

  entity_form_submit_build_entity('qa_question', $qa, $form, $form_state);
  qa_question_save($qa);

  if (module_exists('path')) {
    qa_path_save('qa_question', $qa);
  }

  // If pathauto is enabled we create an alias automatically.
  if (module_exists('pathauto') && !empty($form_state['values']['path']['pathauto'])) {

    // If path doesn't exist already insert, else update.
    $conditions = array('source' => 'qa_question/' . $qa->id);
    $alias = path_load($conditions);

    if (empty($alias)) {
      qa_create_alias($qa, 'insert');
    }
    else {
      qa_create_alias($qa, 'update');
    }
  }


  $qa_question_uri = entity_uri('qa_question', $qa);
  $form_state['redirect'] = $qa_question_uri['path'];
  drupal_set_message(t('Question %title saved.', array('%title' => entity_label('qa_question', $qa))));
}

/**
 * Delete function for Question entities.
 */
function qa_question_form_submit_delete($form, &$form_state) {
  $qa_question = $form_state['qa_question'];
  $qa_question_uri = entity_uri('qa_question', $qa_question);
  $form_state['redirect'] = $qa_question_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function qa_delete_form($form, &$form_state, $qa) {
  $form_state['qa_question'] = $qa;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['qa_question_type_id'] = array(
    '#type' => 'value',
    '#value' => entity_id('qa_question', $qa),
  );

  $qa_question_uri = entity_uri('qa_question', $qa);
  return confirm_form($form,
    t('Are you sure you want to delete qa %title?', array('%title' => entity_label('qa_question', $qa))),
    $qa_question_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function qa_delete_form_submit($form, &$form_state) {
  $qa = $form_state['qa_question'];
  qa_question_delete($qa);

  drupal_set_message(t('Question %title deleted.', array('%title' => entity_label('qa_question', $qa))));

  $form_state['redirect'] = '<front>';
}

/**
 * Add Answer entity form.
 */
function qa_add_answer($type) {
  $qa_answer_type = qa_question_types($type);

  $qa_answer = entity_create('qa_answer', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('qa_answer_type', $qa_answer_type))));

  $output = drupal_get_form('qa_answer_form', $qa_answer);

  return $output;
}

/**
 * Question Form.
 */
function qa_answer_form($form, &$form_state, $qa_answer) {
    
  $form_state['qa_answer'] = $qa_answer;

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $qa_answer->uid,
  );

  field_attach_form('qa_answer', $qa_answer, $form, $form_state);

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Answer'),
  );

  // Show Delete button if we edit qa.
  $id = entity_id('qa_answer', $qa_answer);
  if (!empty($id) && qa_access('edit', $qa_answer)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('qa_answer_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Answer submit handler.
 */
function qa_answer_form_submit($form, &$form_state) {
  $qa_answer = $form_state['qa_answer'];
  entity_form_submit_build_entity('qa_answer', $qa_answer, $form, $form_state);
  qa_answer_save($qa_answer);
  $uri = 'qa_question/' . $qa_answer->qid;
  $form_state['redirect'] = $uri;
  drupal_set_message(t('Answer saved.'));
}

/**
 * Answer delete handler.
 */
function qa_answer_form_submit_delete($form, &$form_state) {
  $qa_answer = $form_state['qa_answer'];
  $qa_answer_uri = entity_uri('qa_answer', $qa_answer);
  $form_state['redirect'] = $qa_answer_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function qa_answer_delete_form($form, &$form_state, $qa_answer) {
  $form_state['qa_answer'] = $qa_answer;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['qa_answer_type_id'] = array(
    '#type' => 'value',
    '#value' => entity_id('qa_answer', $qa_answer),
  );

  $qa_answer_uri = entity_uri('qa_answer', $qa_answer);
  return confirm_form($form,
    t('Are you sure you want to delete qa %title?', array('%title' => entity_label('qa_answer', $qa_answer))),
    $qa_answer_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function qa_answer_delete_form_submit($form, &$form_state) {
  $qa = $form_state['qa_question'];
  qa_question_delete($qa);

  drupal_set_message(t('Question %title deleted.', array('%title' => entity_label('qa_question', $qa))));

  $form_state['redirect'] = '<front>';
}

/**
 * Generates the qa question type editing form.
 */
function qa_answer_type_form($form, &$form_state, $qa_answer_type, $op = 'edit') {
  if ($op == 'clone') {
    $qa_answer_type->label .= ' (cloned)';
    $qa_answer_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $qa_answer_type->label,
    '#description' => t('The human-readable name of this qa question type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($qa_answer_type->type) ? $qa_answer_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $qa_answer_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'qa_answer_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this qa question type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($qa_answer_type->description) ? $qa_answer_type->description : '',
    '#description' => t('Description about the qa answer type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Answer Type'),
    '#weight' => 40,
  );

  if (!$qa_answer_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete qa question type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('qa_answer_type_form_submit_delete'),
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing qa_question_type.
 */
function qa_answer_type_form_submit(&$form, &$form_state) {
  $qa_answer_type = entity_ui_form_submit_build_entity($form, $form_state);

  // Save and go back.
  qa_answer_type_save($qa_answer_type);

  // Redirect user back to list of qa question types.
  $form_state['redirect'] = 'admin/structure/qa-answer-types';
}

/**
 * Answer delete redirect.
 */
function qa_answer_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/qa-answer-types/' . $form_state['qa_question_type']->type . '/delete';
}

/**
 * Question type delete form.
 */
function qa_answer_type_form_delete_confirm($form, &$form_state, $qa_answer_type) {
  $form_state['qa_question_type'] = $qa_answer_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['qa_question_type_id'] = array(
    '#type' => 'value',
    '#value' => entity_id('qa_question_type', $qa_answer_type),
  );

  return confirm_form($form,
    t('Are you sure you want to delete qa question type %title?', array('%title' => entity_label('qa_question_type', $qa_answer_type))),
    'qa_answer/' . entity_id('qa_question_type', $qa_answer_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Question type delete form submit handler.
 */
function qa_answer_type_form_delete_confirm_submit($form, &$form_state) {
  $qa_answer_type = $form_state['qa_question_type'];
  qa_question_type_delete($qa_answer_type);

  watchdog('qa_question_type', '@type: deleted %title.', array('@type' => $qa_answer_type->type, '%title' => $qa_answer_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $qa_answer_type->type, '%title' => $qa_answer_type->label)));

  $form_state['redirect'] = 'admin/structure/qa-question-types';
}

/**
 * Creates a path fieldset
 */
function qa_path_settings($entity_type, $entity, &$form) {

  //$url_prefix = str_replace('_', '-', $entity_type);
  $id = entity_id($entity_type, $entity);

  $path = array();
  if (!empty($id)) {

    $conditions = array('source' => $entity_type . '/' . $id);
    $path = path_load($conditions);

    if ($path === FALSE) {
      $path = array();
    }
  }
  $path += array(
    'pid' => NULL,
    'source' => isset($id) ? $entity_type . '/' . $id : NULL,
    'alias' => '',
    'language' => LANGUAGE_NONE,
  );

  $form['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('URL path settings'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($path['alias']),
    '#group' => 'additional_settings',
    '#attributes' => array(
      'class' => array('path-form'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'path') . '/path.js'),
    ),
    '#access' => user_access('create url aliases') || user_access('administer url aliases'),
    '#weight' => 30,
    '#tree' => TRUE,
    '#element_validate' => array('path_form_element_validate'),
  );
  $form['path']['alias'] = array(
    '#type' => 'textfield',
    '#title' => t('URL alias'),
    '#default_value' => $path['alias'],
    '#maxlength' => 255,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Optionally specify an alternative URL by which this content can be accessed. For example, type "about" when writing an about page. Use a relative path and don\'t add a trailing slash or the URL alias won\'t work.'),
  );
  $form['path']['pid'] = array('#type' => 'value', '#value' => $path['pid']);
  $form['path']['source'] = array('#type' => 'value', '#value' => $path['source']);
  $form['path']['language'] = array('#type' => 'value', '#value' => $path['language']);
}

function qa_path_save($entity_type, $entity) {

  $id = entity_id($entity_type, $entity);

  if (isset($entity->path)) {
    $path = $entity->path;
    $path['alias'] = trim($path['alias']);
    // Only save a non-empty alias.
    if (!empty($path['alias'])) {
      // Ensure fields for programmatic executions.
      $path['source'] = $entity_type . '/' . $id;
      $path['language'] = LANGUAGE_NONE;
      path_save($path);
    }
  }
}

