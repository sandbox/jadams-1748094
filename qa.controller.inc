<?php

/**
 * @file
 * Controller classes for Sections, Questions & Answers.
 */

/**
 * Section Controller
 */
class QaSectionController extends EntityAPIController {

  /**
   * Implements EntityAPIController.
   */
  public function create(array$values = array()) {
    global $user;
    $values += array(
      'title' => '',
      'description' => '',
      'question_type' => 'default_question_type',
      'answer_type' => 'default_answer_type',
      'ask_roles' => '',
      'answer_roles' => '',
      'active' => 1,
      'question_teaser' => 0,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  /**
   * Implements EntityAPIController.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    global $user;
    $section = $entity;

    // Create author field.
    $author = user_load($section->uid);
    $name = theme('username', array('account' => $author));
    $created = format_date($section->created);
    $content['author'] = array(
      '#type' => 'item',
      '#markup' => t('By !name on @created', array('!name' => $name, '@created' => $created)),
    );

    // Create "Ask a question form".
    if (qa_role_check($user, $section->ask_roles) && $section->active) {
      module_load_include('inc', 'qa', 'qa.admin');
      $form = drupal_get_form('qa_question_mini_form', $section);
      $content['form'] = array(
        '#type' => 'item',
        '#markup' => render($form),
      );
    }

    // Really need a better way to do this. Debating whether or not to just have
    // views be a dependency and then get rid of this query all together.
    if (!module_exists('views')) {
      // Create list of questions for this section.
      if ($view_mode == 'full' || ($view_mode == 'teaser' && $section->question_teaser)) {
        $output = '<h2>Questions</h2>';
        $output .= qa_question_list($section->id);
        $content['questions'] = array(
          '#markup' => $output,
        );
      }
    }

    // Theme this section.
    $content += array(
      '#theme' => 'qa_section',
      '#section' => $section,
      '#view_mode' => $view_mode,
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Implements EntityAPIControllerExportable.
   */
  public function save($entity, DatabaseTransaction$transaction = NULL) {
    $entity->changed = REQUEST_TIME;
    parent::save($entity, $transaction);
  }
}

/**
 * Question Controller.
 */
class QaQuestionController extends EntityAPIController {

  /**
   * Implements EntityAPIController.
   */
  public function create(array$values = array()) {
    global $user;
    $values += array(
      'title' => '',
      'active' => 1,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  /**
   * Implements EntityAPIController.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    global $user;
    $question = $entity;
    $section = qa_section_load($question->sid);

    // Create author field.
    $author = user_load($question->uid);
    $name = theme('username', array('account' => $author));
    $created = format_date($question->created);
    $content['author'] = array(
      '#type' => 'item',
      '#markup' => t('By !name on @created', array('!name' => $name, '@created' => $created)),
    );

    // Only Show the link on full view mode.
    if ($view_mode == 'full') {
      $section_link = $section->uri();
      $content['section_link'] = array(
        '#type' => 'item',
        '#markup' => l(t('Go back to @section', array('@section' => $section->title)), $section_link['path']),
      );
    }

    // Create Answer Form.
    if (($view_mode == 'full' || ($view_mode == 'teaser' && $question->answer_teaser)) && qa_role_check($user, $section->answer_roles) && $question->active) {
      module_load_include('inc', 'qa', 'qa.admin');
      // Set question type based on section settings.
      $qa_answer = entity_create('qa_answer', array('type' => $section->answer_type, 'qid' => $question->id));
      $form = drupal_get_form('qa_answer_form', $qa_answer);
      $content['form'] = array(
        '#title' => t('Answer @question', array('@question' => $question->title)),
        '#label_display' => 'above',
        '#type' => 'item',
        '#markup' => render($form),
      );
    }

    // Really need a better way to do this. Debating whether or not to just have
    // views be a dependency and then get rid of this query all together.
    if (!module_exists('views')) {
      // Create list of answers related to this question.
      if ($view_mode == 'full' || ($view_mode == 'teaser' && $question->answer_teaser)) {
        $output = '<h2>Answers</h2>';
        $output .= qa_answers_list($question->id);
        $content['answers'] = array(
          '#markup' => $output,
        );
      }
    }

    // Theme the question.
    $content += array(
      '#theme' => 'qa_question',
      '#question' => $question,
      '#view_mode' => $view_mode,
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Implements EntityAPIControllerExportable.
   */
  public function save($entity, DatabaseTransaction$transaction = NULL) {
    $entity->changed = REQUEST_TIME;
    parent::save($entity, $transaction);
  }
}

/**
 * Answer Controller.
 */
class QaAnswerController extends EntityAPIController {

  /**
   * Implements EntityAPIController.
   */
  public function create(array$values = array()) {
    global $user;
    $values += array(
      'active' => 1,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  /**
   * Implements EntityAPIController.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    global $user;
    $answer = $entity;
    $question = qa_question_load($answer->qid);

    // Create author field.
    $author = user_load($answer->uid);
    $name = theme('username', array('account' => $author));
    $created = format_date($answer->created);
    $content['author'] = array(
      '#type' => 'item',
      '#markup' => t('By !name on @created', array('!name' => $name, '@created' => $created)),
    );

    if (module_exists('votingapi') && user_access('vote answers')) {

      drupal_add_library('system', 'drupal.ajax');
      module_load_include('inc', 'qa', 'qa.vote');

      // Build Vote Widget.
      $votes = qa_answer_get_votes($answer);
      $votes == 1 ? $votes = $votes . ' Vote' : $votes = $votes . ' Votes';
      $content['vote'] = array(
        '#type' => 'item',
        '#markup' => theme('qa_answer_vote_widget', array('answer' => $answer, 'votes' => $votes)),
      );
    }

    // Permissions for these.
    if (!$question->best_answer && ($user->uid == $answer->uid)) {
      $url = 'qa_best_answer/answer/' . $answer->id . '/add';
      $content['best_answer'] = array(
        '#type' => 'item',
        '#markup' => l(t('Choose as Best Answer'), $url),
      );
    }
    elseif ($question->best_answer && $answer->best_answer && $user->uid == $answer->uid) {
      $url = 'qa_best_answer/answer/' . $answer->id . '/remove';
      $content['best_answer'] = array(
        '#type' => 'item',
        '#markup' => l(t('Remove as Best Answer'), $url),
      );
    }

    // Theme the answer.
    $content += array(
      '#theme' => 'qa_answer',
      '#answer' => $answer,
      '#view_mode' => $view_mode,
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Implements EntityAPIControllerExportable.
   */
  public function save($entity, DatabaseTransaction$transaction = NULL) {
    $entity->changed = REQUEST_TIME;
    parent::save($entity, $transaction);
  }
}

/**
 * Question Type Controller.
 */
class QaQuestionTypeController extends EntityAPIControllerExportable {

  /**
   * Implements EntityAPIControllerExportable.
   */
  public function create(array$values = array()) {
    $values += array(
      'label' => '',
      'description' => '',
    );
    return parent::create($values);
  }

  /**
   * Implements EntityAPIControllerExportable.
   */
  public function save($entity, DatabaseTransaction$transaction = NULL) {
    $entity->changed = REQUEST_TIME;
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * Answer Type Controller.
 */
class QaAnswerTypeController extends EntityAPIControllerExportable {

  /**
   * Implements EntityAPIControllerExportable.
   */
  public function create(array$values = array()) {
    $values += array(
      'label' => '',
      'description' => '',
    );
    return parent::create($values);
  }

  /**
   * Implements EntityAPIControllerExportable.
   */
  public function save($entity, DatabaseTransaction$transaction = NULL) {
    $entity->changed = REQUEST_TIME;
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * Question class.
 */
class QaQuestion extends Entity {

  /**
   * Implements Entity.
   */
  protected function defaultLabel() {
    return $this->title;
  }

  /**
   * Implements Entity.
   */
  protected function defaultUri() {
    return array('path' => 'qa_question/' . $this->identifier());
  }
}

/**
 * Section class.
 */
class QaSection extends QaQuestion {

  /**
   * Implements Entity.
   */
  protected function defaultLabel() {
    return $this->title;
  }

  /**
   * Implements Entity.
   */
  protected function defaultUri() {
    return array('path' => 'qa_section/' . $this->identifier());
  }
}

/**
 * Answer class.
 */
class QaAnswer extends QaQuestion {

  /**
   * Implements Entity.
   */
  protected function defaultUri() {
    return array('path' => 'qa_answer/' . $this->identifier());
  }
}

/**
 * Question Type class.
 */
class QaQuestionType extends Entity {
  public $type;
  public $label;
  public $weight = 0;

  /**
   * Implements Entity.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'qa_question_type');
  }

  /**
   * Implements Entity.
   */
  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}

/**
 * Answer Type class.
 */
class QaAnswerType extends Entity {
  public $type;
  public $label;
  public $weight = 0;

  /**
   * Implements Entity.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'qa_answer_type');
  }

  /**
   * Implements Entity.
   */
  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}

